package uz.upay.demo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication



import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args)
	}

}



@RestController
public class HelloController {

	@RequestMapping("/")
	public String hello() {
		return "Hello, world";
	}

	@RequestMapping("/api")
	public String api() {
		return "Hello, first my ip";
	}
}
@RestController
public class ApiController {

	}